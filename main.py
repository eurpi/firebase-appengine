import webapp2
import json
import requests
import requests_toolbelt.adapters.appengine

# Use the App Engine Requests adapter. This makes sure that Requests uses URLFetch
requests_toolbelt.adapters.appengine.monkeypatch()

import firebase_admin
from firebase_admin import credentials
from firebase_admin import auth

cred = credentials.Certificate('%REPLACE-WITH-YOUR-FIREBASE-JSON-KEY-FILENAME%.json')
default_app = firebase_admin.initialize_app(cred)

callback = '' #jQuery callback

def lastReturn(self, dict, r, m):
    try:
        dict['r'] = r
        dict['m'] = m
        self.error(r)
        self.response.out.write(callback + '[' + json.dumps(dict) + ']')
        return r #success
    except:
        dict['r'] = 500
        dict['m'] = 'Exception in lastReturn'
        self.error(r)
        self.response.out.write(callback + '[' + json.dumps(dict) + ']')
        return 500

def authorize(self):
    try:
        id_token = self.request.headers['Authorization'].split(' ').pop()
        decoded_token = auth.verify_id_token(id_token)
        if not decoded_token:
            return 401 #unauthorized
        return decoded_token['uid']
    except:
        return 401 #unauthorized
        
class MainHandler(webapp2.RequestHandler):
    def get(self):
        try:
            dict = {}
            uid = authorize(self)
            if (uid == 401):
                lastReturn(self, dict, uid, 'Unothorized to perform this action')            
                return
            
            user = auth.get_user(uid)
            
            dict['display_name'] = user.display_name
            dict['email'] = user.email
            dict['phone_number'] = user.phone_number
            dict['photo_url'] = user.photo_url
            
            lastReturn(self, dict, 200, 'User Data Successfully Retrieved')            
            
        except:
            lastReturn(self, dict, 500, 'Exception in MainHandler')
            
app = webapp2.WSGIApplication([
    ('/rest/api.*', MainHandler)
], debug=True)
