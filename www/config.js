var config = {
    apiKey: "%TO_BE_REPLACED%",
    authDomain: "%TO_BE_REPLACED%",
    databaseURL: "%TO_BE_REPLACED%",
    projectId: "%TO_BE_REPLACED%",
    storageBucket: "%TO_BE_REPLACED%",
    messagingSenderId: "%TO_BE_REPLACED%"
};
firebase.initializeApp(config);

// Google OAuth Client ID, needed to support One-tap sign-up.
// Set to null if One-tap sign-up is not supported.
 var CLIENT_ID = '%YOUR_CLIENT_ID%';
 var BACKEND_URL = 'http://localhost:9999';