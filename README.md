# Firebase Authentication with Google App Engine and Firebase Admin

This is a small working example of how you can use Firebase for user authentication to your Google App Engine application, using Firebase Admin. Users can login to the application on the frontend using Google, Facebook or Email. Once logged in, the token is sent to App Engine, where it is verified, and the user information is retrieved from Firebase, then returned to the frontend for display.

> P.S: The steps below are tailored for a Windows 10 machine


## Getting Started

Clone this repository on your local machine, and follow the steps mentioned below to make it work.


### Prerequisites

* Create a [Firebase project](https://firebase.google.com/docs/web/setup)

* Install [Google Cloud SDK (App Engine) for Python 2.7 Standard environment](https://cloud.google.com/appengine/docs/standard/python/download)

* Make sure [PIP](https://pip.pypa.io/en/stable/installing/) is working on your machine (should be installed with Cloud SDK and the corresponding Python) 


### Installing

1. Navigate to [Firebase Console](https://console.firebase.google.com) of your project, and in the **Project Overview** landing page, click on the icon **Add Firebase to your web app** 

2. Copy the information within `var config = {...}` and paste it in the corresponding place under the file **www/config.js** 

3. Open CMD on your machine and navigate to the location of the cloned repository, then run the following:

	```
	pip install -r requirements.txt -t lib
	```

4. Navigate to [Firebase Console](https://console.firebase.google.com) of your project, open **Project Settings**, then **Service Accounts**

5. Under *Firebase Admin SDK* click on *Generate Private Key* and download the *_JSON file_*, then move it under the root directory

6. Open the file **main.py** and replace the section **_%REPLACE-WITH-YOUR-FIREBASE-JSON-KEY-FILENAME%_** with the filename of your JSON file

7. Start `dev_appserver.py` by opening CMD and navigating to the root cloned folder, then run the following:

	```
	python "%REPLACE_BY_PATH_TO_GOOGLE_APPENGINE%\dev_appserver.py" --port=9999 app.yaml
	```
	
8. In the broswer, navigate to [http://localhost:9999](http://localhost:9999) and enjoy the experience! :smiley:
